package com.example;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class BackToStockServiceImplTest {

    BackToStockServiceImpl backToStockService;
    @BeforeMethod
    public void setUp() {
        backToStockService = new BackToStockServiceImpl();
    }

    @Test
    public void testSubscribe() {
        User user = new User("test", true, 10);
        Product msft_eq = new Product("MSFT us", ProductCategory.DIGITAL);
        Product ibm = new Product("IBM us", ProductCategory.DIGITAL);
        backToStockService.subscribe(user, msft_eq);
        backToStockService.subscribe(user, ibm);
        backToStockService.subscribe(user, msft_eq);
        Assert.assertEquals(2, backToStockService.getNumberOfExistingProject());
    }

    @Test
    public void testSubscribedUsers() {
        User user = new User("test", true, 10);
        Product msft_eq = new Product("MSFT us", ProductCategory.DIGITAL);
        Product ibm = new Product("IBM us", ProductCategory.DIGITAL);
        backToStockService.subscribe(user, msft_eq);
        backToStockService.subscribe(user, ibm);
        List<User> users = backToStockService.subscribedUsers(ibm);
        Assert.assertEquals(users.size(), 1);
        Assert.assertEquals(users.get(0), user);
    }

    @Test
    public void testSubscribedUsersPriority() {
        User user = new User("test", true, 10);
        User user2 = new User("test2", false, 71);
        User user3 = new User("test3", true, 71);
        User user4 = new User("test4", false, 10);
        User user5 = new User("test5", true, 10);
        Product msft_eq = new Product("MSFT us", ProductCategory.DIGITAL);
        Product ibm = new Product("IBM us", ProductCategory.DIGITAL);
        Product med = new Product("Med us", ProductCategory.MEDICAL);
        backToStockService.subscribe(user, msft_eq);
        backToStockService.subscribe(user, ibm);
        backToStockService.subscribe(user2, ibm);
        backToStockService.subscribe(user3, ibm);
        backToStockService.subscribe(user4, ibm);
        backToStockService.subscribe(user5, med);
        backToStockService.subscribe(user5, ibm);
        List<User> users = backToStockService.subscribedUsers(ibm);
        Assert.assertEquals(users.get(0), user);
        Assert.assertEquals(users.get(1), user3);
        Assert.assertEquals(users.get(2), user5);
        Assert.assertEquals(users.get(3), user2);
    }
}