package com.example;

import java.util.*;
import java.util.stream.Collectors;

public class BackToStockServiceImpl implements BackToStockService{
    private final Map<Product,LinkedList<User>> usersAndProducts;
    private static final int eldersAge = 70;
    public BackToStockServiceImpl() {
        usersAndProducts = new LinkedHashMap<>();
    }



    /**
     * Adding new users and products, unique of product for user preserved
     * @param user user to add
     * @param product for specific product
     */
    @Override
    public void subscribe(User user, Product product) {
        boolean check = usersAndProducts.containsKey(product);
        if(!check){
            usersAndProducts.put(product, new LinkedList<>(Collections.singletonList(user)));
            return;
        }
        List<User> users = usersAndProducts.get(product);
        boolean contains = users.contains(user);
        if(contains){
            return;
        }
        users.add(user);
    }

    /**
     * This method returns prioritized list of users for specified product.
     * the premium user has a high priority
     *     users elder than 70 years old has:
     *     high priority for medical products
     *     medium priority for all categories
     *     FIFO for all the rest
     *     LinkedHashSet is used in order to preserve order and unique
     * @param product key for users list
     * @return prioritized list of users for specified product.
     */
    @Override
    public List<User> subscribedUsers(Product product) {
        List<User> users = usersAndProducts.get(product);
        if (users == null){
            return new ArrayList<>();
        }
        HashSet<User> newUsers = new LinkedHashSet<>();
        newUsers.addAll(getPremiumUsers(users));
        newUsers.addAll(getElderUsers(users, product));
        newUsers.addAll(getOtherElderUsers(users));
        getOther(newUsers, users);

        return List.copyOf(newUsers);
    }



    /**
     * Checking product capacity
     * @return product capacity
     */
    @Override
    public int getNumberOfExistingProject() {
        return usersAndProducts.size();
    }

    private List<User> getPremiumUsers(List<User> persons){
        return persons.stream().filter(User::isPremium).collect(Collectors.toList());
    }

    private List<User> getElderUsers(List<User> persons, Product product){
        return persons.stream().filter(p -> p.getAge() > eldersAge && product.getCategory() == ProductCategory.MEDICAL).collect(Collectors.toList());
    }

    private List<User> getOtherElderUsers(List<User> persons){
        return persons.stream().filter(p -> p.getAge() > eldersAge).collect(Collectors.toList());
    }

    private void getOther(Set<User> persons, List<User> all){
        persons.addAll(all);
    }
}
