package com.example;

import java.util.List;

interface BackToStockService {

    void subscribe(User user, Product product);

    List<User> subscribedUsers(Product product);

    int getNumberOfExistingProject();
}
